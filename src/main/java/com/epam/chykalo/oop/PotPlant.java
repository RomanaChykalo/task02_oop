package com.epam.chykalo.oop;

public interface PotPlant {
    void growUp(int maxValue);
    default boolean hasRoot(){
        return true;
    }
}
