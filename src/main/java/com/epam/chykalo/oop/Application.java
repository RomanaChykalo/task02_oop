package com.epam.chykalo.oop;

import com.epam.chykalo.oop.com.epam.chykalo.oop.impl.CutFlowerComposition;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static <KindOfCutFlower> void main(String[] args) {
        CutFlowerComposition composition = new CutFlowerComposition("BirthdayBouquet");
        List<CutFlowerComposition.KindOfCutFlower> cutFlowers = new ArrayList<>();
        composition.addAmountOfSomeKindFlower(cutFlowers, 1, CutFlowerComposition.KindOfCutFlower.DAISY);
        composition.addAmountOfSomeKindFlower(cutFlowers, 2, CutFlowerComposition.KindOfCutFlower.ROSE);
        composition.smell();
        System.out.println("Now we will create " + composition.getCompositionName() + " composition, which consists of "
                + cutFlowers);
        composition.countCompositionFlower(cutFlowers);
        System.out.println("General cost of the bouquet is: " + composition.calculatePrice(cutFlowers));
        composition.sortByPrice(cutFlowers);
        CutFlowerComposition.getFlowersAmount(cutFlowers);

    }
}
