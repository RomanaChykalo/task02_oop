package com.epam.chykalo.oop.com.epam.chykalo.oop.impl;

import com.epam.chykalo.oop.Plant;

import java.util.*;
import java.util.stream.Collectors;

public class CutFlowerComposition extends Plant {
    private String compositionName;
    private List<KindOfCutFlower> cutFlowers = new ArrayList<>();

    public CutFlowerComposition(String compositionName) {
        setBlooming(true);
        this.compositionName = compositionName;
    }

    public void smell() {
        System.out.println("Cut flowers have a tangible smell");
    }

    public void addAmountOfSomeKindFlower(List<KindOfCutFlower> cutFlowers, int amount, KindOfCutFlower cutFlower) {
        List<KindOfCutFlower> duplicates = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            duplicates.add(cutFlower);
        }
        cutFlowers.addAll(duplicates);

    }

    public void countCompositionFlower(List<KindOfCutFlower> cutFlowers) {
        Map<KindOfCutFlower, Long> countMap = cutFlowers.stream()
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        countMap.forEach((k, v) -> System.out.println(k + " we put : " + v + " times"));

    }

    public int calculatePrice(List<KindOfCutFlower> cutFlowers) {
        int sum = 0;
        for (KindOfCutFlower cutFlower : cutFlowers) {
            sum += cutFlower.getPrice();
        }
        return sum;
    }

    public void sortByPrice(List<KindOfCutFlower> cutFlowers) {
        Collections.sort(cutFlowers, Comparator.comparingInt(p -> p.price));
        System.out.println("Our sorted by value bouquet is look like :" + cutFlowers);
    }

    public static void getFlowersAmount(List<KindOfCutFlower> cutFlowers) {
        System.out.println("General flower's amount in our composition is: " + cutFlowers.size());
    }

    public String getCompositionName() {
        return compositionName;
    }


    public enum KindOfCutFlower {
        DAISY(10),
        EUSTOMA(30),
        PION(15),
        ROSE(20),
        TULIP(8),
        LILY(40);
        int price;

        KindOfCutFlower(int p) {
            price = p;
        }

        public int getPrice() {
            return price;
        }
    }

}