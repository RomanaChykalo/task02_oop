package com.epam.chykalo.oop.com.epam.chykalo.oop.impl;

import com.epam.chykalo.oop.Plant;
import com.epam.chykalo.oop.PotPlant;

public class Palm extends Plant implements PotPlant {

    private int price;

    Palm(int price) {
        this.price = price;
        boolean blooming = false;
    }
    @Override
    public int getPrice() {
        return price;
    }
    @Override
    public void growUp(int maxValue) {
        System.out.println("Palm's maximum palm height is " + maxValue + " meters");
    }

    @Override
    public void smell() {
        System.out.println("Palm trees are odorless");
    }
}
