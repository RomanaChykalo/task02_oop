package com.epam.chykalo.oop;

public abstract class Plant {
    private int price;
    private boolean blooming;
    public abstract void smell();

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isBlooming() {
        return blooming;
    }

    public void setBlooming(boolean blooming) {
        this.blooming = blooming;
    }

}
